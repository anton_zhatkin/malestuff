# -*- coding: utf-8 -*-
# import robot_vastane as AI


def loo_väljak():			 	# hiljem võib lasta suvalst mängustaadiumi panna
    väljak = [[] for i in range(8)]
    for i in range(2, 6):
        for g in range(8):
            väljak[i].append(0)  # minu meelest null on parim tähistus tühjale kohale
    väljak[1] = ["e" for i in range(8)]
    väljak[6] = ["E" for i in range(8)]
    väljak[0] = ["v1", "r1", "o1", "l", "k", "o2", "r2", "v2"]  # mustad nupud väikse tähega
    väljak[7] = ["V1", "R2", "O1", "L", "K", "O2", "R2", "V1"]  # valged suurega
    return väljak

nupud = dict(zip(["V","L","O","H","K","E"],["vanker","lipp","oda","hobune","kuningas","ettur"]))


def loo_väljak2():
    väljak = [[] for i in range(8)]
    for i in range(2, 6):
        for g in range(8):
            väljak[i].append(0)
    m = "must"
    v = "valge"
    väljak[1] = [ettur(i, 7, m) for i in range(8)]
    väljak[6] = [ettur(i, 2, v) for i in range(8)]

    väljak[0] = [vanker(1, 8, m), hobune(2, 8, m), oda(3, 8, m), lipp(4, 8, m), kuningas(5, 8, m)]
    väljak[0] += [oda(6, 8, m), hobune(7, 8, m), vanker(8, 8, m)]
    väljak[7] = [vanker(1, 8, v), hobune(2, 8, v), oda(3, 8, v), lipp(4, 8, v), kuningas(5, 8, v)]
    väljak[7] += [oda(6, 8, v), hobune(7, 8, v), vanker(8, 8, v)]
    return väljak

väljak = loo_väljak()   # füüsiline väljak


def t(arv):
    return (8-arv)


def p(asi):
    return [print(i) for i in asi]


def legaalsed(self):
    ok = []
    print(self.nupp)
    if self.nupp == "H":
        ok += [[self.x + 1, self.y + 2], [self.x - 1, self.y + 2]]
        ok += [[self.x + 1, self.y - 2], [self.x - 1, self.y - 2]]
        ok += [[self.x + 2, self.y - 1], [self.x + 2, self.y + 1]]
        ok += [[self.x - 2, self.y - 1], [self.x - 2, self.y + 1]]
    elif self.nupp == "V":
        for i in range(8):
            ok.append([self.x, i])
        for i in range(8):
            ok.append([i, self.y])
    elif self.nupp == "O":
        for i in range(8):
            ok.append([self.x - i, self.y - i])
            ok.append([self.x + i, self.y + i])
            ok.append([self.x + i, self.y - i])
            ok.append([self.x - i, self.y + i])
    elif self.nupp == "L":
        for i in range(8):
            ok.append([self.x - i, self.y - i])
            ok.append([self.x + i, self.y + i])
            ok.append([self.x + i, self.y - i])
            ok.append([self.x - i, self.y + i])
        for i in range(8):
            ok.append([self.x, i])
        for i in range(8):
            ok.append([i, self.y])
    elif self.nupp == "E":
        if self.y == 2:
            ok.append([self.x, self.y+2])
        elif self.y == 7:
            ok.append([self.x, self.y-2])
        if self.värv == "valge":
            ok += [[self.x + i, self.y + 1] for i in range(-1, 2)]
        else:
            ok += [[self.x + i, self.y - 1] for i in range(-1, 2)]
        try:
            x1, y1 = ok[-1]
            if väljak[t(y1)][x1] != 0:
                ok.remove(ok[-1])
        except:
            pass
        try:
            x2, y2 = ok[-2]
            if väljak[t(y2)][x2] != 0:
                ok.remove(ok[-2])
        except:
            pass
    elif self.nupp == "K":
        ok += [[self.x + i, self.y + 1] for i in range(-1, 2)]
        ok += [[self.x + i, self.y] for i in range(-1, 2)]
        ok += [[self.x + i, self.y - 1] for i in range(-1, 2)]
    print(ok)
    for i in ok:
        if (((i[0] or i[1]) > 8) or ((i[0] or i[1]) < 1)):
            ok.remove(i)
        if ((i[0] == self.x) and (i[1] == self.y)):
            ok.remove(i)
    print(ok)
    return ok


def kntrlli_liikumiseks(ok, kuhu):
    x, y = kuhu
    if len(ok) == 0:
        return False
    elif kuhu not in ok:
        return False
    return True


def asenda(nupp, kuhu, kust):
    x1, y1 = kuhu
    x, y = kust
    if väljak2[t(y)][x] == 0:
        return
    värv = väljak2[t(y)][x].värv
    global väljak
    väljak[t(y1)][x1] = nupp
    väljak[t(y)][x] = 0
    väljak2[t(y)][x] = 0
    exec("väljak2[t(y1)][x1] = "+nupud[nupp]+"(x1, y1, värv)")
    return


class malend:
    def __init__(self, x, y, värv):
        self.x = x
        self.y = y
        self.värv = värv

    def liiguta(self, kuhu):
        kuhu = list(kuhu)
        kust = [self.x, self.y]
        if kntrlli_liikumiseks(self.ok, kuhu):
            asenda(self.nupp, kuhu, kust)
        else:
            return
        return


class hobune(malend):
    def liiguta(self, kuhu):
        kuhu = list(kuhu)
        kust = [self.x, self.y]
        if kntrlli_liikumiseks(self.ok, kuhu):
            asenda(self.nupp, kuhu, kust)
        else:
            return False
        return

    def __init__(self, x, y, värv):
        self.nupp = "H"
        super(hobune, self).__init__(x, y, värv)
        if värv == "valge":
            self.pilt = "nupud/V_H.png"
        else:
            self.pilt = "nupud/M_H.png"
        self.ok = legaalsed(self)


class vanker(malend):
    def liiguta(self, kuhu):
        kuhu = list(kuhu)
        kust = [self.x, self.y]
        if kntrlli_liikumiseks(self.ok, kuhu):
            asenda(self.nupp, kuhu, kust)
        else:
            return False
        return

    def __init__(self, x, y, värv):
        self.nupp = "V"
        super(vanker, self).__init__(x, y, värv)
        if värv == "valge":
            self.pilt = "nupud/V_V.png"
        else:
            self.pilt = "nupud/M_V.png"
        self.ok = legaalsed(self)


class oda(malend):
    def liiguta(self, kuhu):
        kuhu = list(kuhu)
        kust = [self.x, self.y]
        if kntrlli_liikumiseks(self.ok, kuhu):
            asenda(self.nupp, kuhu, kust)
        else:
            return False
        return

    def __init__(self, x, y, värv):
        self.nupp = "O"
        super(oda, self).__init__(x, y, värv)
        if värv == "valge":
            self.pilt = "nupud/V_O.png"
        else:
            self.pilt = "nupud/M_O.png"
        self.ok = legaalsed(self)


class ettur(malend):
    def liiguta(self, kuhu):
        kuhu = list(kuhu)
        kust = [self.x, self.y]
        if kntrlli_liikumiseks(self.ok, kuhu):
            asenda(self.nupp, kuhu, kust)
        else:
            return False
        return

    def __init__(self, x, y, värv):
        self.nupp = "E"
        super(ettur, self).__init__(x, y, värv)
        if värv == "valge":
            self.pilt = "nupud/V_E.png"
        else:
            self.pilt = "nupud/M_E.png"
        self.ok = legaalsed(self)


class lipp(malend):
    def liiguta(self, kuhu):
        kuhu = list(kuhu)
        kust = [self.x, self.y]
        if kntrlli_liikumiseks(self.ok, kuhu):
            asenda(self.nupp, kuhu, kust)
        else:
            return False
        return

    def __init__(self, x, y, värv):
        self.nupp = "L"
        super(lipp, self).__init__(x, y, värv)
        if värv == "valge":
            self.pilt = "nupud/V_L.png"
        else:
            self.pilt = "nupud/M_L.png"
        self.ok = legaalsed(self)


class kuningas(malend):
    def vangerda(self, kuidas):  # VANGERDAMINE, võiks täitsa kunagi võib-olla olla
        pass

    def liiguta(self, kuhu):
        kuhu = list(kuhu)
        kust = [self.x, self.y]
        if kntrlli_liikumiseks(self.ok, kuhu, self.värv):
            asenda(self.nupp, kuhu, kust)
        else:
            return False
        return

    def __init__(self, x, y, värv):
        self.nupp = "K"
        super(kuningas, self).__init__(x, y, värv)
        if värv == "valge":
            self.pilt = "nupud/V_K.png"
        else:
            self.pilt = "nupud/M_K.png"
        self.ok = legaalsed(self)

väljak2 = loo_väljak2()  # väljak, kus elementideks on klassiisendid

#print(väljak2)
#[print(i) for i in väljak]
