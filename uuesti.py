# -*- coding: utf-8 -*-
from collections import namedtuple as nt

mlnd = nt("malend", ["nupp", "värv", "x", "y"])
nupud = dict(zip(["V","L","O","H","K","E"],["vanker","lipp","oda","hobune","kuningas","ettur"]))


def loo_väljak():			 	# hiljem võib lasta suvalst mängustaadiumi panna
    väljak = [[] for i in range(8)]
    for i in range(2, 6):
        for g in range(8):
            väljak[i].append(0)  # minu meelest null on parim tähistus tühjale kohale
    väljak[1] = ["E" for i in range(8)]
    väljak[6] = ["E" for i in range(8)]
    väljak[0] = ["V", "R", "O", "L", "K", "O", "R", "V"]  # mustad nupud väikse tähega
    väljak[7] = ["V", "R", "O", "L", "K", "O", "R", "V"]  # valged suurega
    return väljak


def loo_väljak2():
    väljak2 = [[] for i in range(8)]
    r = -1
    for rida in väljak:
        r += 1
        for nupp in rida:
            n = rida.index(nupp)
            if nupp != 0:
                if r > 4:
                    värv = "valge"
                else:
                    värv = "must"
                väljak2[r].append(mlnd(nupp, värv, n+1, 8-r))
            else:
                väljak2[r].append(0)
    return väljak2

def p(asi):
    return [print(i) for i in asi]


väljak = loo_väljak()
väljak2 = loo_väljak2()
p(väljak)
p(väljak2)