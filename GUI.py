# -*- coding: utf-8 -*-
import pygame as pg
import OO_projekt as bnd


def t(arv):
    return (bnd.t(arv))


def vaheta_käik(käik):
    if käik == "valge":
        return "must"
    else:
        return "valge"

käik = "valge"
BRGNDY = (140, 0, 26)
BLACK = (0, 0, 0)
GREEN = (0, 255, 0)
RED = (255, 0, 0)
WHITE = (0xFF, 0xFF, 0xFF)
YELLOW = (255, 255, 0)
BLUE = (65, 105, 255)
pg.init()
size = (1024, 768)
screen = pg.display.set_mode(size)
pg.display.set_caption("Lahe mäng, jou.")
clock = pg.time.Clock()
mäng = True
font_menüü = pg.font.SysFont("Times New Roman", 40, True, False)
font_mäng = pg.font.SysFont("Courier New", 20, True, False)
bg = pg.image.load("brgndy.jpg").convert()
koordid = [[] for i in range(8)]
kohad = [[] for i in range(8)]
for i in range(8):  # võiks teha list-comprehensioniga, aga lebola ei luba
    for g in range(8):
        koordid[i].append([[20 + g*80, 95 + g*80], [20+i*80, 95+i*80]])
[print(i) for i in koordid]
for i in range(8):
    for g in range(8):
        kohad[i].append([g, 8-i])
[print(i) for i in kohad]


def joonista_väljak():
    for i in range(8):
        for g in range(8):
            if (i+g) % 2 == 0:
                värv = WHITE
            else:
                värv = BLUE
            pg.draw.rect(screen, värv, [20 + g*80, 20 + i*80, 75, 75])


def joonista_ajalugu():
    tekst = font_mäng.render("vot iz app, main semu ", True, BLACK)
    screen.blit(tekst, [720, 30])


def joonista_menüü():
    screen.blit(bg, [0, 0])
    tekst = font_menüü.render("Mängi!", True, WHITE)
    screen.blit(tekst, [500, 200])


def joonista_mäng():
    joonista_väljak()
    pg.draw.polygon(screen, BLACK, [[700, 20], [1000, 20], [1000, 654], [700, 654]], 4)
    pg.draw.rect(screen, WHITE, [702, 23, 296, 630])
    joonista_ajalugu()
    for rida in väljak2_2:
        for nupp in rida:
            if nupp != 0:
                pilt = pg.image.load(nupp.pilt).convert_alpha()
                screen.blit(pilt, [27 + 80*rida.index(nupp), 27 + 80*bnd.väljak2.index(rida)])
           # print(rida.index(nupp))
hiir = []


def kasruudus(koht):
    x, y = koht
    for rida in koordid:
        for ruut in rida:
            if ((ruut[0][0] < x < ruut[0][1]) and (ruut[1][0] < y < ruut[1][1])):
                a = [kohad[koordid.index(rida)][rida.index(ruut)][0], kohad[koordid.index(rida)][rida.index(ruut)][1]]
                print(a)
                return a
    return False
while mäng:
    for event in pg.event.get():
        if event.type == pg.QUIT:
            mäng = False
            break
        elif event.type == pg.MOUSEBUTTONDOWN:
            if len(hiir) > 2:
                hiir = []
            hiir.append(pg.mouse.get_pos())
        if len(hiir) == 2:
            if (kasruudus(hiir[0]) and kasruudus(hiir[1])):
                hiir = [kasruudus(hiir[0]), kasruudus(hiir[1])]
                print(hiir)
                nupp1 = bnd.väljak2[t(hiir[0][1])][hiir[0][0]]
                print(nupp1)
                print(nupp1.värv)
                print(nupp1.nupp)
                print(nupp1.ok)
                if nupp1 != 0 and käik == nupp1.värv:
                    nupp1.liiguta(hiir[1])
                    käik = vaheta_käik(käik)
                    print(käik)
                hiir = []
                break
    väljak2_2 = bnd.väljak2
#############################
    screen.fill(BRGNDY)
    joonista_mäng()
    for rida in koordid:
        for ruut in rida:
            x1, x2 = ruut[0]
            y1, y2 = ruut[1]
            pg.draw.polygon(screen, YELLOW, [[x1, y1], [x2, y1], [x2, y2], [x1, y2]], 5)
    pg.display.flip()
    clock.tick(8)
pg.quit()




"""
import sys
from PyQt4.QtGui import *

def loo_GUI():

    a = QApplication(sys.argv)
    w = QMainWindow()
    w.resize(1024, 768)
    w.setWindowTitle("Lahe mäng!")

    mainMenu = w.menuBar()
    mainMenu.setNativeMenuBar(False)
    fileMenu = mainMenu.addMenu("&Menüü")

    mängiNupp = QAction(QIcon("exit24.png"), "Uus mäng", w)
    mängiNupp.setShortcut("Ctrl+N")
    mängiNupp.triggered.connect(w.close)
    fileMenu.addAction(mängiNupp)

    exitButton = QAction(QIcon("exit24.png"), "Välju", w)
    exitButton.setShortcut("Ctrl+Q")
    exitButton.triggered.connect(w.close)
    fileMenu.addAction(exitButton)


    tekst_kst = QLineEdit(w)
    tekst_kst.move(200, 150)
    tekst_kst.resize(300, 50)

    w.show()
    sys.exit(a.exec_())
    return
"""
